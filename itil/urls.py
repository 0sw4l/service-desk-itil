"""itil URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import logout
from django.shortcuts import redirect
from Apps.Ajustes.views import login


def salir(request):
    logout(request)
    return redirect('log_in')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^chaining/', include('smart_selects.urls')),
    #   url de login
    url(r'^$', login, name='log_in'),
    url(r'^login/', login, name='log_in_'),
    url(r'^salir', salir, name='salir'),
    # urls de ajustes
    url(r'^ajustes/', include('Apps.Ajustes.urls', namespace='ajustes')),
    # urls de soporte
    url(r'^soporte/', include('Apps.Soporte.urls', namespace='soporte')),

    url(r'^empleados/', include('Apps.Empleados.urls', namespace='empleado')),

]
