from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models

# Create your models here.
from Apps.Ajustes.models import Empleado
from Apps.Soporte.models import Incidente, Solucion


class NotificacionSolucion(models.Model):

    empleado = models.ForeignKey(Empleado)
    incidente = models.ForeignKey(Incidente)
    solucion = models.ForeignKey(Solucion)
    encargado = models.ForeignKey(User, related_name='encargado', editable=False)
    hora = models.TimeField(auto_now_add=True)
    fecha = models.DateField(auto_now_add=True)
    visto = models.BooleanField(default=False)

