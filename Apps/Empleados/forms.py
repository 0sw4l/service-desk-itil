from django import forms
from django.contrib.auth.forms import UserCreationForm
from Apps.Ajustes.forms import BaseForm
from Apps.Ajustes.models import ElementoConfiguracion, Empleado
from Apps.Soporte.models import Incidente


class IncidenteUsuarioForm(BaseForm):

    form_title = 'Incidente'
    time_widget = forms.widgets.TimeInput(attrs={'class': 'time-pick'})
    valid_time_formats = ['%H:%M', '%I:%M%p', '%I:%M %p']
    hora = forms.TimeField(widget=time_widget, help_text='ex: 10:30AM', input_formats=valid_time_formats)
    elemento_afectado = forms.ModelChoiceField(
        queryset=ElementoConfiguracion.objects.all(), empty_label=None)

    class Meta:
        model = Incidente
        fields = ('elemento_afectado',
                  'categoria',
                  'prioridad',
                  'asunto',
                  'descripcion',
                  'hora',
                  'fecha'
                  )
        exclude = ('cerrado', 'soluciones')


class EmpleadoForm(UserCreationForm):

    form_title = 'Incidente'

    first_name = forms.CharField(required=True, label='Nombres')
    last_name = forms.CharField(required=True, label='Apellidos')
    email = forms.EmailField(required=True, label='Correo Electronico')

    class Meta:
        model = Empleado
        fields = ('username',
                  'first_name',
                  'last_name',
                  'departamento',
                  'cargo',
                  'email')