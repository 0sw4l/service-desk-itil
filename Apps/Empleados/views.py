from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

# Create your views here.
from Apps.Ajustes.models import ElementoConfiguracion, Empleado
from Apps.Ajustes.views import ListBase
from Apps.Empleados.forms import IncidenteUsuarioForm, EmpleadoForm
from Apps.Empleados.models import NotificacionSolucion
from Apps.Soporte.models import Incidente, Solucion


class IncidenteUsuarioView(ListBase):
    template_name = 'listados/soporte/incidentes.html'
    paginate_by = 10
    queryset = Incidente.objects.all()
    model = Incidente

    def get_context_data(self, **kwargs):
        context = super(IncidenteUsuarioView, self).get_context_data(**kwargs)
        context['incidentes_lista'] = True
        context['incidentes'] = Incidente.objects.filter(usuario=self.request.user)
        return context


@login_required
def crear_incidente_usuario(request):
    form = IncidenteUsuarioForm()
    elemento_afectado = ElementoConfiguracion.objects.filter(usuario=request.user)
    form.fields['elemento_afectado'].queryset = elemento_afectado
    if request.POST:
        form = IncidenteUsuarioForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('empleado:incidentes')
    context = {
        'form': form,
        'accion': 'Crear',
    }
    return render(request, 'Main/forms/baseForm.html', context)


@login_required
def crear_usuario(request):
    form = EmpleadoForm()
    if request.POST:
        form = EmpleadoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('empleado:usuarios')
    context = {
        'form': form,
        'accion': 'Crear',
    }
    return render(request, 'Main/forms/baseForm.html', context)


@login_required
def usuarios(request):
    usuarios = Empleado.objects.order_by('-id')
    return render(request, 'listados/empleados/empleados.html', {
        'usuarios': usuarios
    })


@login_required
def notificaciones(request):
    alertas = None
    if hasattr(request.user, 'empleado'):
        user = Empleado.objects.get(id=request.user.id)
        alertas = NotificacionSolucion.objects.filter(
            empleado=user
        )
        print(alertas)
    return render(request, 'listados/empleados/notificaciones.html', {
        'notificaciones': alertas
    })


@login_required
def ver_solucion(request, **kwargs):
    id = kwargs.get('id')
    solucion_notificacion = NotificacionSolucion.objects.get(solucion_id=id)
    if hasattr(request.user, 'empleado') and solucion_notificacion.visto is False:
        solucion_notificacion.visto = True
        solucion_notificacion.save()
        user = Empleado.objects.get(id=request.user.id)
        user.notificaciones -= 1
        user.save()
    solucion = Solucion.objects.filter(id=id)
    context = {
        'soluciones': solucion,
        'incidente': solucion_notificacion.incidente
    }
    return render(request, 'listados/soporte/soluciones.html', context)


@login_required
def incidentes_usuario(request):
    objetos = Incidente.objects.filter(
        usuario=request.user.empleado
    )
    lista = True if objetos.count() > 0 else False
    return render(request, 'listados/soporte/incidentes.html', {
        'incidentes': objetos,
        'incidentes_lista': lista

    })


@login_required
def incidentes_solucionados(request):
    incidentes = Incidente.objects.filter(
        usuario=request.user,
        cerrado=True
    )
    return render(request, 'listados/soporte/incidentes.html', {
        'incidentes': incidentes
    })


@login_required
def incidentes_no_solucionados(request):
    incidentes = Incidente.objects.filter(
        usuario=request.user,
        cerrado=False
    )
    return render(request, 'listados/soporte/incidentes.html', {
        'incidentes': incidentes
    })