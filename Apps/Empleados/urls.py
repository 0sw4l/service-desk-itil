from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from views import IncidenteUsuarioView, crear_incidente_usuario, crear_usuario, usuarios
from views import notificaciones, ver_solucion, incidentes_solucionados, incidentes_usuario
from views import incidentes_no_solucionados

urlpatterns = [
    url(r'^incidentes', login_required(IncidenteUsuarioView.as_view()), name='incidentes'),
    url(r'^registrar-incidente', crear_incidente_usuario, name='crear_incidente'),
    url(r'^registrar-usuario', crear_usuario, name='crear_usuario'),
    url(r'^usuarios', usuarios, name='usuarios'),
    url(r'^notificaciones', notificaciones, name='notificaciones'),
    url(r'^solucion/(?P<id>\w+)', ver_solucion, name='ver_solucion'),
    url(r'^incidentes-solucionados', incidentes_solucionados, name='ver_incidentes_solucionados'),
    url(r'^incidentes-no-solucionados', incidentes_no_solucionados, name='ver_incidentes_no_solucionados'),
    url(r'^incidentes-usuario', incidentes_usuario, name='incidentes_usuario'),
]