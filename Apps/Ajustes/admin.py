from django.contrib import admin
from models import ElementoConfiguracion, Empleado, Departamento, Tipo, Cargo
from models import Proveedor, Marca, ProveedorMarca, Producto
# Register your models here.


class AdminBase(admin.ModelAdmin):
    list_display = ['id', 'nombre']


@admin.register(Tipo)
class TipoAdmin(AdminBase):
    pass


@admin.register(Departamento)
class DepartamentoAdmin(AdminBase):
    pass


@admin.register(Cargo)
class CargoAdmin(AdminBase):
    pass


@admin.register(Empleado)
class EmpleadoAdmin(AdminBase):
    list_display = ['id', 'first_name', 'last_name', 'departamento', 'cargo']


@admin.register(ElementoConfiguracion)
class ElementoConfiguracionAdmin(admin.ModelAdmin):
    list_display = ['serial', 'tipo', 'producto', 'departamento',
                    'usuario', 'fecha_configuracion']


@admin.register(Proveedor)
class ProveedorAdmin(AdminBase):
    pass


@admin.register(Marca)
class MarcaAdmin(AdminBase):
    pass


@admin.register(ProveedorMarca)
class ProveedorMarcaAdmin(admin.ModelAdmin):
    list_display = ['id', 'proveedor_x', 'marca']


@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
    list_display = ['id', 'tipo', 'proveedor', 'marca', 'descripcion']

