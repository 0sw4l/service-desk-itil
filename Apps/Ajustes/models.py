from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from smart_selects.db_fields import ChainedForeignKey


class MainModel(models.Model):

    nombre = models.CharField(max_length=50)

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.nombre


class Tipo(MainModel):

    class Meta:
        verbose_name = 'Tipo'
        verbose_name_plural = 'Tipos'


class Departamento(MainModel):

    class Meta:
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'


class Cargo(MainModel):

    class Meta:
        verbose_name = 'Cargo'
        verbose_name_plural = 'Cargos'


class Empleado(User):

    cargo = models.ForeignKey(Cargo)
    departamento = models.ForeignKey(Departamento)
    notificaciones = models.PositiveIntegerField(editable=False, default=0)

    class Meta:
        verbose_name = 'Empleado'
        verbose_name_plural = 'Empleados'

    def __unicode__(self):
        return self.get_full_name()


class Proveedor(MainModel):

    class Meta:
        verbose_name = 'Proveedor'
        verbose_name_plural = 'Proveedores'


class Marca(MainModel):

    class Meta:
        verbose_name = 'Marca'
        verbose_name_plural = 'Marcas'


class ProveedorMarca(models.Model):

    proveedor_x = models.ForeignKey(Proveedor)
    marca = models.ForeignKey(Marca)

    class Meta:
        verbose_name = 'Proveedor Marca'
        verbose_name_plural = 'Marcas y Proveedores'
        unique_together = ['proveedor_x', 'marca']

    def __unicode__(self):
        return self.marca.nombre


class Producto(models.Model):

    tipo = models.ForeignKey(Tipo)
    proveedor = models.ForeignKey(Proveedor)
    marca = ChainedForeignKey(
        ProveedorMarca,
        chained_field='proveedor',
        chained_model_field='proveedor_x'
    )
    descripcion = models.TextField()

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'

    def __unicode__(self):
        d = '{0} marca {1} : descripcion {2} : proveedor {3}'
        return d.format(self.tipo, self.marca, self.descripcion, self.proveedor)


class ElementoConfiguracion(models.Model):

    serial = models.CharField(max_length=50, unique=True)
    tipo = models.ForeignKey(Tipo)
    producto = ChainedForeignKey(
        Producto,
        chained_field='tipo',
        chained_model_field='tipo',
    )
    departamento = models.ForeignKey(Departamento)
    usuario = ChainedForeignKey(
        Empleado,
        chained_field='departamento',
        chained_model_field='departamento'
    )
    fecha_configuracion = models.DateField()
    incidentes = models.PositiveIntegerField(default=0, editable=False)

    class Meta:
        verbose_name = 'Elemento de Configuracion'
        verbose_name_plural = 'Elementos de Configuracion'
        unique_together = ['tipo', 'producto', 'usuario']

    def __unicode__(self):
        d = '{0} : marca {1} {2} : a cargo del usuario {3}'
        return d.format(self.tipo, self.producto.marca, self.producto.descripcion, self.usuario)
