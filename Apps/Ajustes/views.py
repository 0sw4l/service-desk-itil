from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.auth.decorators import login_required
# Create your views here.
from django.views.generic import ListView
from Apps.Ajustes.forms import ElementoConfiguracionForm, ProductoForm
from Apps.Ajustes.models import Tipo, ElementoConfiguracion, Producto, Proveedor
from Apps.Soporte.models import Incidente


def login(request):
    context = {'error': False}
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                if request.user.is_superuser:
                    return redirect('ajustes:inicio')
                else:
                    return redirect('empleado:incidentes')
            else:
                context = {'msj': 'El usuario ha sido desactivado', 'error': True}
        else:
            context = {'msj': 'Usuario o Password incorrecta', 'error': True}
    return render(request, 'Main/sesion/login.html', context)


@login_required
def inicio(request):
    incidentes = Incidente.objects.filter(cerrado=False).count()
    elementos = ElementoConfiguracion.objects.all().count()
    solucionados = Incidente.objects.all().filter(cerrado=True).count()
    context = {
        'incidentes': incidentes,
        'elementos': elementos,
        'solucionados': solucionados
    }
    return render(request, 'index.html', context)


class ListBase(LoginRequiredMixin, ListView):
    pass


class ElementosView(ListBase):

    model = ElementoConfiguracion
    template_name = 'listados/ajustes/elemento_de_configuracion.html'
    paginate_by = 10
    context_object_name = 'elementos'
    queryset = ElementoConfiguracion.objects.all().order_by('-id')


@login_required
def nuevo_elemento(request):
    form = ElementoConfiguracionForm()
    if request.POST:
        form = ElementoConfiguracionForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('ajustes:elementos')
    context = {
        'form': form,
        'accion': 'Crear'
    }
    return render(request, 'Main/forms/baseForm.html', context)


@login_required
def nuevo_producto(request):
    form = ProductoForm()
    if request.POST:
        form = ProductoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('ajustes:productos')
    context = {
        'form': form,
        'accion': 'Crear'
    }
    return render(request, 'Main/forms/baseForm.html', context)


class ProductoView(ListBase):

    template_name = 'listados/ajustes/productos.html'
    model = Producto
    paginate_by = 10
    context_object_name = 'productos'
    queryset = Producto.objects.all().order_by('-id')

    def get_context_data(self, **kwargs):
        context = super(ProductoView, self).get_context_data(**kwargs)
        context['proveedor'] = False
        return context


def origen(request, **kwargs):
    id_proveedor = kwargs.get('id')
    proveedor = Proveedor.objects.get(id=id_proveedor)
    productos = Producto.objects.filter(proveedor_id=proveedor.id)
    return render(request, 'listados/ajustes/productos.html', {
        'proveedor_data': proveedor,
        'productos': productos,
        'proveedor': True
    })
