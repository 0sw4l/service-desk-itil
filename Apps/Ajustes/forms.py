from django import forms
from models import ElementoConfiguracion, Producto


class BaseForm(forms.ModelForm):
    form_title = ''


class ElementoConfiguracionForm(BaseForm):

    form_title = 'Elemento de configuracion'

    class Meta:
        model = ElementoConfiguracion
        fields = ('__all__')
        exclude = ('incidentes',)


class ProductoForm(BaseForm):

    form_title = 'Producto'

    class Meta:
        model = Producto
        fields = ('__all__')

