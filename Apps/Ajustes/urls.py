from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from .views import inicio, ElementosView, nuevo_elemento, nuevo_producto, ProductoView
from .views import origen

urlpatterns = [
    url(r'^inicio/', inicio, name='inicio'),
    url(r'^elementos/', login_required(ElementosView.as_view()), name='elementos'),
    url(r'^nuevo-elemento', nuevo_elemento, name='nuevo_elemento'),
    url(r'^nuevo-producto', nuevo_producto, name='nuevo_producto'),
    url(r'^productos', ProductoView.as_view(), name='productos'),
    url(r'^origen/(?P<id>\w+)', origen, name='p_p')
]
