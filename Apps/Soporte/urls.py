from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from .views import IncidenteView, crear_incidente, crear_incidente_elemento
from .views import crear_solucion, incidente_elemento, solucion_incidente, solucionar_incidente
from .views import ProveedorView, crear_proveedor, crear_marca, MarcaView, editar_marca
from .views import proveedor_marca

urlpatterns = [
    url(r'^incidentes/', login_required(IncidenteView.as_view()), name='incidentes'),
    url(r'^crear-incidente', crear_incidente, name='crear_incidente'),
    url(r'^crear-solucion/(?P<id>\w+)/', crear_solucion, name='crear_solucion'),
    url(r'^incidente/(?P<id_objeto>\w+)/', incidente_elemento, name='incidentes_objeto'),
    url(r'^solucion-incidente/(?P<id_incidente>\w+)/', solucion_incidente, name='solucion_incidente'),
    url(r'^solucionar-incidente/(?P<id_incidente>\w+)/', solucionar_incidente, name='solucionar_incidente'),
    url(r'^incidente-elemento/(?P<id_elemento>\w+)/', crear_incidente_elemento, name='incidente_elemento'),
    url(r'^proveedores/', ProveedorView.as_view(), name='proveedores'),
    url(r'^crear-proveedor', crear_proveedor, name='crear_proveedor'),
    url(r'^crear-marca', crear_marca, name='crear_marca'),
    url(r'^marcas', MarcaView.as_view(), name='marcas'),
    url(r'editar-marca/(?P<id>\w+)/', editar_marca, name='editar_marca'),
    url(r'^asignar-proveedor-marca', proveedor_marca, name='proveedor_marca'),

]