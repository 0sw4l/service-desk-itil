from __future__ import unicode_literals

from django.db import models
from smart_selects.db_fields import ChainedForeignKey
from Apps.Ajustes.models import ElementoConfiguracion, MainModel, Empleado


# Create your models here.


class Prioridad(MainModel):

    horas_de_resolucion = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'Prioridad'
        verbose_name_plural = 'Prioridades'

    def __unicode__(self):
        data = '{0}, {1} horas'.format(self.nombre, self.horas_de_resolucion)
        return data


class CategoriaIncidente(MainModel):

    class Meta:
        verbose_name_plural = 'Cateogrias de incidentes'
        verbose_name = 'Categoria de incidente'


class Incidente(models.Model):

    usuario = models.ForeignKey(Empleado, editable=False)
    elemento_afectado = models.ForeignKey(ElementoConfiguracion)
    categoria = models.ForeignKey(CategoriaIncidente)
    asunto = models.CharField(max_length=50)
    descripcion = models.TextField()
    cerrado = models.BooleanField(default=False)
    prioridad = models.ForeignKey(Prioridad)
    hora = models.TimeField()
    fecha = models.DateField()
    soluciones = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'Incidente'
        verbose_name_plural = 'Incidentes'

    def __unicode__(self):
        return "incidente #{0}".format(self.id)

    def save(self, *args, **kwargs):
        if not self.id:
            self.usuario = self.elemento_afectado.usuario
            self.elemento_afectado.incidentes += 1
            self.elemento_afectado.save()
        super(Incidente, self).save(*args, **kwargs)


class Solucion(models.Model):

    incidente = models.ForeignKey(Incidente)
    elemento_afectado = models.ForeignKey(ElementoConfiguracion)
    descripcion = models.TextField()

    class Meta:
        verbose_name = 'Solucion'
        verbose_name_plural = 'Soluciones'

    def save(self, *args, **kwargs):
        if not self.id:
            self.incidente.soluciones += 1
            self.incidente.save()
        super(Solucion, self).save(*args, **kwargs)

    def __unicode__(self):
        return "solucion al problema incidente #{0}".format(self.incidente.id)

