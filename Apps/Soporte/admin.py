from django.contrib import admin
from models import Prioridad, CategoriaIncidente, Incidente, Solucion
from Apps.Ajustes.admin import AdminBase
# Register your models here.


@admin.register(Prioridad)
class PrioridadAdmin(admin.ModelAdmin):

    list_display = ['id', 'nombre', 'horas_de_resolucion']


@admin.register(CategoriaIncidente)
class CategoriaIncidenteAdmin(AdminBase):
    pass


@admin.register(Incidente)
class IncidenteAdmin(admin.ModelAdmin):
    list_display = ['id', 'asunto', 'descripcion', 'cerrado',
                    'prioridad', 'elemento_afectado', 'categoria', 'hora',
                    'fecha']


@admin.register(Solucion)
class SolucionAdmin(admin.ModelAdmin):
    list_display = ['id', 'incidente', 'descripcion']
