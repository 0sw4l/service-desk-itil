from django import forms
from Apps.Ajustes.forms import BaseForm
from Apps.Ajustes.models import ElementoConfiguracion, Proveedor, Marca, ProveedorMarca
from Apps.Soporte.models import Incidente, Solucion


class IncidenteForm(BaseForm):

    form_title = 'Incidente'
    time_widget = forms.widgets.TimeInput(attrs={'class': 'time-pick'})
    valid_time_formats = ['%H:%M', '%I:%M%p', '%I:%M %p']
    hora = forms.TimeField(widget=time_widget, help_text='ex: 10:30AM', input_formats=valid_time_formats)
    elemento_afectado = forms.ModelChoiceField(
        queryset=ElementoConfiguracion.objects.all(), empty_label=None)

    class Meta:
        model = Incidente
        fields = ('__all__')
        exclude = ('cerrado', 'soluciones')


class SolucionForm(BaseForm):

    form_title = 'Solucion'
    incidente = forms.ModelChoiceField(
        queryset=Incidente.objects.all(), empty_label=None)
    elemento_afectado = forms.ModelChoiceField(
        queryset=ElementoConfiguracion.objects.all(), empty_label=None)

    class Meta:
        model = Solucion
        fields = ('incidente', 'elemento_afectado', 'descripcion')


class ProveedorForm(BaseForm):

    form_title = 'Proveedor'

    class Meta:
        model = Proveedor
        fields = ('__all__')


class MarcaForm(BaseForm):

    form_title = 'Marca'

    class Meta:
        model = Marca
        fields = ('__all__')


class ProveedorMarcaForm(BaseForm):

    form_title = 'Proveedor de marca'

    class Meta:
        model = ProveedorMarca
        fields = ('__all__')

