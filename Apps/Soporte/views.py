from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from Apps.Ajustes.models import ElementoConfiguracion, Proveedor, Marca, ProveedorMarca
from Apps.Ajustes.views import ListBase
from Apps.Empleados.models import NotificacionSolucion
from Apps.Soporte.forms import IncidenteForm, SolucionForm, ProveedorForm, MarcaForm, ProveedorMarcaForm
from Apps.Soporte.models import Incidente, Solucion

"""
    elemento = ElementoConfiguracion.objects.get(id=id)
    queryset = Incidente.objects.filter(elemento_afectado=elemento)
"""


class IncidenteView(ListBase):
    template_name = 'listados/soporte/incidentes.html'
    model = Incidente
    paginate_by = 10
    context_object_name = 'incidentes'
    queryset = Incidente.objects.all().order_by('-id')

    def get_context_data(self, **kwargs):
        context = super(IncidenteView, self).get_context_data(**kwargs)
        context['incidentes_lista'] = True
        return context


@login_required
def crear_incidente(request):
    form = IncidenteForm()
    if request.POST:
        form = IncidenteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('soporte:incidentes')
    context = {
        'form': form,
        'accion': 'Crear',
    }
    return render(request, 'Main/forms/baseForm.html', context)


@login_required
def crear_incidente_elemento(request, **kwargs):
    id = kwargs.get('id_elemento')
    form = IncidenteForm()
    elemento_afectado = ElementoConfiguracion.objects.filter(id=id)
    form.fields['elemento_afectado'].queryset = elemento_afectado
    if request.POST:
        form = IncidenteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('soporte:incidentes_objeto', id)
    context = {
        'form': form,
        'accion': 'Crear'
    }
    return render(request, 'Main/forms/baseForm.html', context)


@login_required
def crear_solucion(request, **incidente):
    id = incidente.get('id')
    form = SolucionForm()

    incidente_top = Incidente.objects.get(id=id)
    usuario = incidente_top.elemento_afectado.usuario
    incidente = Incidente.objects.filter(id=id)
    form.fields['incidente'].queryset = incidente

    elemento_afectado = ElementoConfiguracion.objects.filter(id=incidente_top.elemento_afectado.id)
    form.fields['elemento_afectado'].queryset = elemento_afectado

    if request.POST:
        form = SolucionForm(request.POST)
        if form.is_valid():
            form.save()
            solucion = Solucion.objects.filter(incidente=incidente_top).order_by('-id')[0]
            NotificacionSolucion(
                incidente=incidente_top,
                solucion=solucion,
                empleado=usuario.empleado,
                encargado=request.user
            ).save()
            empleado = usuario
            empleado.notificaciones += 1
            empleado.save()
            return redirect('soporte:solucion_incidente', id)
    context = {
        'form': form,
        'accion': 'Crear'
    }
    return render(request, 'Main/forms/baseForm.html', context)


@login_required
def incidente_elemento(request, **kwargs):
    id = kwargs.get('id_objeto')
    elemento = ElementoConfiguracion.objects.get(id=id)
    incidentes = Incidente.objects.filter(elemento_afectado=elemento)
    context = {
        'incidentes': incidentes,
        'elemento': elemento
    }
    return render(request, 'listados/soporte/incidentes.html', context)


@login_required
def solucion_incidente(request, **kwargs):
    id = kwargs.get('id_incidente')
    incidente = Incidente.objects.get(id=id)
    soluciones = Solucion.objects.filter(incidente=incidente)
    context = {
        'soluciones': soluciones,
        'incidente': incidente
    }
    return render(request, 'listados/soporte/soluciones.html', context)


@login_required
def solucionar_incidente(request, **kwargs):
    id = kwargs.get('id_incidente')
    incidente = Incidente.objects.get(id=id)
    if incidente.soluciones > 0:
        incidente.cerrado = True
        incidente.save()
        return redirect('soporte:incidentes')
    else:
        return render(request, 'listados/soporte/cerrar_incidente_fail.html')


@login_required
def crear_proveedor(request):
    form = ProveedorForm()
    if request.POST:
        form = ProveedorForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('soporte:proveedores')
    context = {
        'form': form,
        'accion': 'Crear',
    }
    return render(request, 'Main/forms/baseForm.html', context)


class ProveedorView(ListBase):
    template_name = 'listados/soporte/proveedores.html'
    model = Proveedor
    paginate_by = 10
    context_object_name = 'proveedores'
    queryset = Proveedor.objects.all().order_by('-id')

    def get_context_data(self, **kwargs):
        context = super(ProveedorView, self).get_context_data(**kwargs)
        return context


@login_required
def crear_marca(request):
    form = MarcaForm()
    if request.POST:
        form = MarcaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('soporte:marcas')
    context = {
        'form': form,
        'accion': 'Crear',
    }
    return render(request, 'Main/forms/baseForm.html', context)


class MarcaView(ListBase):
    template_name = 'listados/soporte/marcas.html'
    model = Marca
    paginate_by = 10
    context_object_name = 'proveedores'
    queryset = Marca.objects.all().order_by('-id')

    def get_context_data(self, **kwargs):
        context = super(MarcaView, self).get_context_data(**kwargs)
        return context


@login_required
def editar_marca(request, **kwargs):
    id = kwargs.get('id')
    marca = Marca.objects.get(id=id)
    form = MarcaForm(instance=marca)
    if request.POST:
        form = MarcaForm(request.POST, instance=marca)
        if form.is_valid():
            form.save()
            return redirect('soporte:marcas')
    context = {
        'form': form,
        'accion': 'Crear',
    }
    return render(request, 'Main/forms/baseForm.html', context)


@login_required
def proveedor_marca(request):
    form = ProveedorMarcaForm()
    if request.POST:
        form = ProveedorMarcaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('soporte:marcas')
    context = {
        'form': form,
        'accion': 'Asignar',
    }
    return render(request, 'Main/forms/baseForm.html', context)
